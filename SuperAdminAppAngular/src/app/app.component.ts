import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SuperAdminAppAngular';

  constructor(private router: Router) {
    console.log("Alo polisia");
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log("Esta Adentro 7u7");
        router.navigate(['/home']);
      } else {
        console.log("No Esta Adentro u.u");
        router.navigate(['/']);
      }
    });

  }

}
