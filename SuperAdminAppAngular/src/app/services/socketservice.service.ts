import { Injectable, NgModule, NgZone } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { RutaServer } from '../RutaApi';

@Injectable({
  providedIn: 'root'
})
export class SocketConfigService extends Socket {
  constructor() {

    super({ url: RutaServer.socket + "admins", options: {} });
  }

}
