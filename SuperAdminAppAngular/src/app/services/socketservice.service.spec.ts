import { TestBed } from '@angular/core/testing';

import { SocketConfigService } from './socketservice.service';

describe('SocketConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketConfigService = TestBed.get(SocketConfigService);
    expect(service).toBeTruthy();
  });
});
