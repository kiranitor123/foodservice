import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: any;
  password: any;
  message: any = "";

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  async onSubmit(){
    try{
      const result = await firebase.auth().signInWithEmailAndPassword(this.email,this.password);
      if(result){
        console.log("Funciona");
      }
    }
    catch(e){
      if(e == "Error: There is no user record corresponding to this identifier. The user may have been deleted."){
        this.message="El correo ingresado no corresponde a ningun usuario verifique su correo";
      }
      else if(e == "Error: The password is invalid or the user does not have a password."){
        this.message="La contraseña es incorrecta";
      }
      else if(e == "Error: The email address is badly formatted."){
        this.message="El correo presenta un formato incorrecto";
      }
    }
  }
}
