import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SocketConfigService } from '../../services/socketservice.service';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {

  items: any = [];

  filteredItems: any = [];

  constructor(private socket:SocketConfigService,) {
    socket.emit('listar-usuarios', null);
    socket.on('respuesta-listado', (data) => {
      console.log(data);
      this.items = Object.assign([], data);
      this.assignCopy();
    });
  }

  ngOnInit() {
  }

  assignCopy(){
    console.log(this.items);
    this.filteredItems = Object.assign([], this.items);
  }

  filterItem(value){
     if(!value){
         this.assignCopy();
     } // when nothing has typed
     console.log(value);
     this.filteredItems = Object.assign([], this.items).filter(
        item => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1
     )
  }

}
