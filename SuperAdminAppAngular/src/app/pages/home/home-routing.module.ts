import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { AdminsComponent } from '../admins/admins.component';
import { DeliveryComponent } from '../delivery/delivery.component';
import { ClientComponent } from '../client/client.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
        {
          path: 'admins',
          component: AdminsComponent
        },
        {
          path: 'delivery',
          component: DeliveryComponent
        },
        {
          path: 'clients',
          component: ClientComponent
        }
      ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
