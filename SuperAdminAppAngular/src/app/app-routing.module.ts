import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: LoginComponent
    },

];

@NgModule({
    imports: [RouterModule.forRoot(routes), FormsModule, ReactiveFormsModule],
    exports: [RouterModule]
})
export class AppRoutingModule { }
