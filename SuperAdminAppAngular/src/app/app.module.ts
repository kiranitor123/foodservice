import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import 'hammerjs';

import { LoginComponent } from './pages/login/login.component';
import { HomeModule } from './pages/home/home.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from "@angular/common";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SocketConfigService } from './services/socketservice.service';
import { Socket, SocketIoModule } from 'ngx-socket-io';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import * as firebase from 'firebase';
import { AdminDetailsComponent } from './pages/admin-details/admin-details.component';
import { RestaurantListComponent } from './pages/restaurant-list/restaurant-list.component';
import { RestaurantComponent } from './pages/restaurant/restaurant.component';
import { MenuComponent } from './pages/menu/menu.component';
var config = {
  apiKey: "AIzaSyCk8CaYIbcKxHEy000iaDnXmK7SEdXx5fA",
  authDomain: "foodservice-appsuperadmin.firebaseapp.com",
  databaseURL: "https://foodservice-appsuperadmin.firebaseio.com",
  projectId: "foodservice-appsuperadmin",
  storageBucket: "foodservice-appsuperadmin.appspot.com",
  messagingSenderId: "1022171636863"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminDetailsComponent,
    RestaurantListComponent,
    RestaurantComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    SocketIoModule,
    AppRoutingModule,
    HomeModule
  ],
  providers: [SocketConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
