import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { PasswordrecoveryComponent } from './pages/passwordrecovery/passwordrecovery.component';
import { RegisterRestaurantComponent } from './pages/register-restaurant/register-restaurant.component';

const routes: Routes = [
  { path: '',  component: LoginComponent},
  { path: 'home',  component: HomeComponent},
  { path: 'passwordrecovery',  component: PasswordrecoveryComponent},
  { path: 'registerrestaurant', component: RegisterRestaurantComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
