import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { RutaServer } from '../RutaApi';

@Injectable({
  providedIn: 'root'
})
export class adminSocketService extends Socket{
  constructor() {
    super({ url: RutaServer.socket + "admins", options: {} });
  }
}

export class restaurantSocketService extends Socket{
  constructor() {
    super({ url: RutaServer.socket + "restaurants", options: {} });
  }
}
