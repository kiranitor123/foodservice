import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AdminAppAngular';

  constructor(private router: Router) {
    console.log('Observador creado.');
    firebase.auth().onAuthStateChanged((user) => {
      if (user) { 
        if (router.url === '/' || router.url === '/passwordrecovery') {
          this.router.navigate(['/home']);
        }
        console.log('Existe una sesion iniciada');
      } else {
        this.router.navigate(['']);
        console.log('No existe una sesion iniciada.');
      }
    });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}

