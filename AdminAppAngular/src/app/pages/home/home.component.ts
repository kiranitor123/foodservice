import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userphotoURL = 'assets/images/defaultuser.png';
  username = 'Username';
  
  constructor() {

  }

  ngOnInit() {
  }

  logout() {
    firebase.auth().signOut().then(() => {
      console.log('Se cerro la sesion');
    }).catch((error) => {
      console.log('Hubo un error al tratar de cerrar sesion');
    });
  }
}
