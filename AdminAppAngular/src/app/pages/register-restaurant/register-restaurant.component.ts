import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-register-restaurant',
  templateUrl: './register-restaurant.component.html',
  styleUrls: ['./register-restaurant.component.scss']
})
export class RegisterRestaurantComponent implements OnInit {

  adminID: any;

  actualdate: any;

  restaurant: any = {
    name: '',
    socialreason: '',
    nit: '',
    created: {
      on: '',
      by: ''
    },
    modified: {
      on: '',
      by: ''
    },
    deleted: {
      on: '',
      by: ''
    },
    branchoffices: [
      {
        adress: '',
        number: [
          ''
        ],
        products: [
          {
            name: '',
            tag: '',
            price: '',
            information: ''
          }
        ],
        ingredient: [
          {
            name: '',
            price: '',
            tag: '',
            information: ''
          }
        ]
      }
    ]
  };

  constructor(private datePipe: DatePipe) {
    this.actualdate = this.datePipe.transform(this.actualdate, 'dd-MM-yyyy');
    console.log(this.actualdate);
  }

  ngOnInit() {
  }

}
