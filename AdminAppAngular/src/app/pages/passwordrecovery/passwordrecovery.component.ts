import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-passwordrecovery',
  templateUrl: './passwordrecovery.component.html',
  styleUrls: ['./passwordrecovery.component.scss']
})
export class PasswordrecoveryComponent implements OnInit {

  email: any;

  constructor() {}

  ngOnInit() {
  }

  send() {
    var auth = firebase.auth();
    auth.sendPasswordResetEmail(this.email).then(() => {
      console.log('Correo de recuperacion enviado.');
    }).catch((error) => {
      console.log('Ocurrio un error al tratar de enviar el correo.');
    });
  }
}
