import { Component, OnInit } from '@angular/core';
import { adminSocketService } from '../../services/socketservice.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  admin: any;

  emailusername: any;
  email:any;
  password: any;
  errormessage: any;

  constructor(private socket:adminSocketService) {
    
  }

  ngOnInit() {
  }

  login() {
    var index = this.emailusername.indexOf( "@" ); 
    if (index === -1){
      console.log('Es un nombre de usuario');
      this.socket.emit('encontrar-admin-por-email', this.emailusername);
      this.socket.on('respuesta-admin-por-email', (data) => {
        console.log(data);
        this.admin = Object.assign([], data);
        this.email = data[0].email;
        this.verify();
      });
    } else {
      console.log('Es un Correo');
      this.email = this.emailusername;
      this.verify();
    }
  }
  
  verify()
  {
    firebase.auth().signInWithEmailAndPassword(this.email,this.password).catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
  }
  
}
