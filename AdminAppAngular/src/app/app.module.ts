import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

// Angular Material y BootStrap
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule, MatButtonModule} from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

// Firebase
import * as firebase from 'firebase';

// Sockets
import { adminSocketService } from './services/socketservice.service';
import { restaurantSocketService } from './services/socketservice.service';
import { Socket, SocketIoModule } from 'ngx-socket-io';

// Pages and Components
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { PasswordrecoveryComponent } from './pages/passwordrecovery/passwordrecovery.component';
import { RegisterRestaurantComponent } from './pages/register-restaurant/register-restaurant.component';


var config = {
  apiKey: "AIzaSyDFu46LBRDsmJMItDtZDyo_nguu5Q7NCC4",
  authDomain: "foodservice-a8058.firebaseapp.com",
  databaseURL: "https://foodservice-a8058.firebaseio.com",
  projectId: "foodservice-a8058",
  storageBucket: "foodservice-a8058.appspot.com",
  messagingSenderId: "991242029492"
};
firebase.initializeApp(config);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PasswordrecoveryComponent,
    RegisterRestaurantComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    HttpClientModule,
    SocketIoModule
  ],
  providers: [adminSocketService,restaurantSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
