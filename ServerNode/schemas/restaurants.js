"use strict"
var mongose = require("mongoose");
var Schema = mongose.Schema;
var RestaurantSchema = Schema({
  name:String,
  phone:String,
  adress:String,
  created:[{on:String},{by:String}],
  modified:[{on:String},{by:String}],
  deleted:[{status:Boolean},{reason:String},{on:String},{by:String}],
  products:[[{_id:String},{name:String},{price:Number},{info:String},{tag:String}]]
})

module.exports = mongose.model("restaurants", RestaurantSchema)
