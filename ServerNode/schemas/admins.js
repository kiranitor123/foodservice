"use strict"
var mongose = require("mongoose");
var Schema = mongose.Schema;
var AdminSchema = Schema({
  name:String,
  username:String,
  password:String,
  email:String,
  phone:Number,
  token:String,
  created:{
    on:String,
    by:String
  },
  modified:{
    on:String,
    by:String
  },
  deleted:{
    status:Boolean,
    reason:String,
    on:String,
    by:String
  }
})

module.exports = mongose.model("admins", AdminSchema)
