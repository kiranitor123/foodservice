"use strict"
var mongose = require("mongoose");
var Schema = mongose.Schema;
var SuperAdminSchema = Schema({
  name:String,
  username:String,
  password:String,
  token:String,
  created:String,
  modified:String,
  deleted:String
})

module.exports = mongose.model("superadmins", SuperAdminSchema)
