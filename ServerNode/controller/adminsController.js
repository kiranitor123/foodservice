"use strict"

var Admin = require("../schemas/admins");

module.exports = async function (io) {

  io.on('connection', async function (socket) {

    console.log("Conectado a socket de Admins");

    socket.on('listar-admins', async (data) => {
      Admin.find({ "deleted.status" : false }, function (error, lista) {
        if (error) {
          // res.status(500).send({ mensaje: "Error al listar" })
        } else {
          if (!lista) {
            //   res.status(404).send({ mensaje: "Error al listar" })
          } else {
            console.log("Lectura Correcta");
            io.to(socket.id).emit('respuesta-listado', lista);
          }
        }
      });
    });

    socket.on('encontrar-admin-por-email', async (data) => {
      Admin.find({ "username" : data }, function (error, lista) {
        if (error) {
          // res.status(500).send({ mensaje: "Error al listar" })
        } else {
          if (!lista) {
            //   res.status(404).send({ mensaje: "Error al listar" })
          } else {
            console.log("Lectura Correcta");
            io.to(socket.id).emit('respuesta-admin-por-email', lista);
          }
        }
      });
    });
  });
};
