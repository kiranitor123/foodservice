"use strict"

var Restaurant = require("../schemas/restaurants");

module.exports = async function (io) {

  io.on('connection', async function (socket) {

    console.log("Conectado a socket de Restaurantes");

    socket.on('listar-restaurantes', async (data) => {
      Admin.find({ "deleted.status": false }, function (error, lista) {
        if (error) {
          // res.status(500).send({ mensaje: "Error al listar" })
        } else {
          if (!lista) {
            //   res.status(404).send({ mensaje: "Error al listar" })
          } else {
            console.log("Lectura Correcta");
            io.to(socket.id).emit('respuesta-listado', lista);
          }
        }
      });
    });
  });
};
