import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterbranchoficceIndividualmenuPage } from './registerbranchoficce-individualmenu.page';

describe('RegisterbranchoficceIndividualmenuPage', () => {
  let component: RegisterbranchoficceIndividualmenuPage;
  let fixture: ComponentFixture<RegisterbranchoficceIndividualmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterbranchoficceIndividualmenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterbranchoficceIndividualmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
