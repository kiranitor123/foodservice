import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterbranchoficceIndividualmenuPage } from './registerbranchoficce-individualmenu.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterbranchoficceIndividualmenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterbranchoficceIndividualmenuPage]
})
export class RegisterbranchoficceIndividualmenuPageModule {}
