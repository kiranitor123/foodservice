import { Component, OnInit } from '@angular/core';
import { User } from 'src/models/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {} as User;
  message: any = "";

  constructor(private afAuth: AngularFireAuth,private router: Router) {
   }

  async login(user:User){
    try{
      const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email,user.password);
      if(result){
        this.router.navigate(['registerrestaurant']);
      }
    }
    catch(e){
      if(e == "Error: There is no user record corresponding to this identifier. The user may have been deleted."){
        this.message="El correo ingresado no corresponde a ningun usuario verifique su correo";
      }
      else if(e == "Error: The password is invalid or the user does not have a password."){
        this.message="La contraseña es incorrecta";        
      }
      else if(e == "Error: The email address is badly formatted."){
        this.message="El correo presenta un formato incorrecto";                
      }
    }
  }

  recoverypassword(){
    this.router.navigate(['recoverypassword']);
  }

  ngOnInit() {
  }

}
