import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterproductPage } from './registerproduct.page';

describe('RegisterproductPage', () => {
  let component: RegisterproductPage;
  let fixture: ComponentFixture<RegisterproductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterproductPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterproductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
