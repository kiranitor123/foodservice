import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterrestaurantPage } from './registerrestaurant.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterrestaurantPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterrestaurantPage]
})
export class RegisterrestaurantPageModule {}
