import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, RouterModule } from '@angular/router';
import { ReplaySubject, Observable } from 'rxjs';

@Component({
  selector: 'app-registerrestaurant',
  templateUrl: './registerrestaurant.page.html',
  styleUrls: ['./registerrestaurant.page.scss'],
})
export class RegisterrestaurantPage implements OnInit {

  @ViewChild("fileinput") fileinput: ElementRef;

  imagen64: string;
  contentEditable: boolean = false;

  constructor(private afAuth: AngularFireAuth,private router: Router) { }

  async signOut(){
    this.afAuth.auth.signOut().then(function() {
      console.log("se deslogeo");
    }).catch(function(error) {
      console.log("Peto");
    });
  }

  toggleEditable(event) {
    if ( event.target.checked ) {
        this.contentEditable = true;
   }
   else{
    this.contentEditable = false;
   }
  }

  abririmagen(){
    this.fileinput.nativeElement.click();
  }

  insertimage(event){
    this.createBASE64(event.srcElement.files[0]).subscribe(data=>{console.log(data);this.imagen64=data})
  }

  createBASE64(imagen: File):Observable<MSBaseReader>{
    console.log("imagen ingresada 3", imagen);
    let base64 = new ReplaySubject<MSBaseReader>(1);
    let Filereader = new FileReader();
    Filereader.onload = event=>{
      base64.next(Filereader.result)
    };
    Filereader.readAsDataURL(imagen);
    return base64;
  }

  agregarsucursal(){
    if(this.contentEditable==true){
      this.router.navigate(['registerbranchoficce-globalmenu']);
    }
    else{
      this.router.navigate(['registerbranchoficce-individualmenu']);
    }
  }

  agregarproducto(){
    this.router.navigate(['registerproduct']);
  }
  ngOnInit() {
  }

}
