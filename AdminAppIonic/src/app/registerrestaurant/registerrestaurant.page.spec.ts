import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterrestaurantPage } from './registerrestaurant.page';

describe('RegisterrestaurantPage', () => {
  let component: RegisterrestaurantPage;
  let fixture: ComponentFixture<RegisterrestaurantPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterrestaurantPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterrestaurantPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
