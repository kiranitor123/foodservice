import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  { path: 'registerrestaurant', loadChildren: './registerrestaurant/registerrestaurant.module#RegisterrestaurantPageModule' },
  { path: 'recoverypassword', loadChildren: './recoverypassword/recoverypassword.module#RecoverypasswordPageModule' },
  { path: 'registerbranchoficce-individualmenu', loadChildren: './registerbranchoficce-individualmenu/registerbranchoficce-individualmenu.module#RegisterbranchoficceIndividualmenuPageModule' },
  { path: 'registerbranchoficce-globalmenu', loadChildren: './registerbranchoficce-globalmenu/registerbranchoficce-globalmenu.module#RegisterbranchoficceGlobalmenuPageModule' },
  { path: 'registerproduct', loadChildren: './registerproduct/registerproduct.module#RegisterproductPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
