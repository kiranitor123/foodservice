import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-recoverypassword',
  templateUrl: './recoverypassword.page.html',
  styleUrls: ['./recoverypassword.page.scss'],
})
export class RecoverypasswordPage implements OnInit {

  email: any;

  constructor() {}

  ngOnInit() {
  }

  send() {
    var auth = firebase.auth();
    auth.sendPasswordResetEmail(this.email).then(() => {
      console.log('Correo de recuperacion enviado.');
    }).catch((error) => {
      console.log('Ocurrio un error al tratar de enviar el correo.');
    });
  }
}