import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegisterbranchoficceGlobalmenuPage } from './registerbranchoficce-globalmenu.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterbranchoficceGlobalmenuPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterbranchoficceGlobalmenuPage]
})
export class RegisterbranchoficceGlobalmenuPageModule {}
