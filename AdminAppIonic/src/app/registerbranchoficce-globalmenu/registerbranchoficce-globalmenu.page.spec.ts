import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterbranchoficceGlobalmenuPage } from './registerbranchoficce-globalmenu.page';

describe('RegisterbranchoficceGlobalmenuPage', () => {
  let component: RegisterbranchoficceGlobalmenuPage;
  let fixture: ComponentFixture<RegisterbranchoficceGlobalmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterbranchoficceGlobalmenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterbranchoficceGlobalmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
